/**
 * This is the root package of the 'Bags' library which is part of the 
 * JavaOverhaulLibrary.
 * This library takes care of the definition of all mutable 'primitive' ADT's,
 * such as {@link com.github.elwinbran.javaoverhaul.bags.Map} and 
 * {@link com.github.elwinbran.javaoverhaul.bags.Stack}.
 * This should ensure that all ADT requirements can always be met and provide
 * a universal interface.
 */
package com.github.elwinbran.javaoverhaul.bags;