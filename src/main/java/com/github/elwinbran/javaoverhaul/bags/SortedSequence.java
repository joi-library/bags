/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

import java.util.Comparator;

/**
 * A sequence of elements that is sorted according to a {@code Comparator}.
 * 
 * TODO: how will comparators be done?
 * 
 * @author Elwin Slokker
 * @param <ElementT>
 */
public interface SortedSequence<ElementT> extends Bag<ElementT>
{
    @Override
    public abstract boolean add(ElementT element);
    
    @Override
    public abstract boolean addAll(Iterable<? extends ElementT> iterable);
    
    @Override
    public abstract boolean contains(ElementT element);
    
    public abstract void remove(ElementT element);
    
    @Deprecated
    public abstract SortedSequence<ElementT> getGreater(ElementT element, 
            Comparator<? extends ElementT> comparator);
    
    @Deprecated
    public abstract SortedSequence<ElementT> getLesser(ElementT element, 
            Comparator<? extends ElementT> comparator);
    
}
