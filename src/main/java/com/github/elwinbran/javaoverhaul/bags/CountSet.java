/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

/**
 * A set that allows duplicate elements and keeps count of how many duplicates
 * there are of a given duplicate.
 * 
 * <br><br>NOTE: {@code CountSet} relies heavily on the 
 * {@link Object#equals(java.lang.Object) equals(Object)} method.
 * Not implementing the {@code equals(Object)} in the element class method may
 * cause problems.
 * 
 * @author Elwin Slokker
 * @param <ElementT>
 */
public interface CountSet<ElementT> extends Bag<ElementT>
{
    /**
     * Will add the element to the set if there is enough space.
     * 
     * @param element
     * @return 
     */
    @Override
    public abstract boolean add(ElementT element);
    
    public abstract boolean add(ElementT element, int multiplicity);
    
    @Override
    public abstract boolean addAll(Iterable<? extends ElementT> iterable);
    
    /**
     * Changes the multiplicity of given element.
     * Might fail when the implementation has a capacity and the change exceeds
     * the capacity.
     * 
     * @param element
     * @param multiplicity
     * @return 
     */
    public abstract boolean set(ElementT element, int multiplicity);
    
    /**
     * Subtracts one of the multiplicity of given element, if it exists.
     * Use {@link #set set(ElementT, int)} to reduce an element multiplicity to 
     * zero. Using this method beyond zero multiplicity may throw exceptions.
     * 
     * @param element 
     */
    public abstract void remove(ElementT element);
    
    /**
     * Removes all elements from the set that are also present in the given 
     * iterable.
     * 
     * @param iterable
     */
    public abstract void remove(Iterable<? extends ElementT> iterable);
    
    @Override
    public abstract boolean contains(ElementT element);
    
    /**
     * The amount of the given element in this set.
     * 
     * @param element
     * @return 
     */
    public abstract int multiplicity(ElementT element);
    
    /**
     * Makes a new multiset that contains all elements of this set and the 
     * given set combined.
     * @param other
     * @return 
     */
    public abstract CountSet<ElementT> union(CountSet<? super ElementT> other);
    
    /**
     * Makes a new multiset that contains all elements from this multiset, that 
     * are not also in the given subtractor set.
     * 
     * @param subtractor
     * @return 
     */
    public abstract CountSet<ElementT> subtractComplement(Set<? super ElementT> subtractor);
    
    /**
     * Makes a new set that only contains elements that are not present in both 
     * this set and the given set (the 'distinct' elements).
     * 
     * @param other
     * @return 
     */
    public abstract CountSet<ElementT> difference(Set<? super ElementT> other);
    
    /**
     * Makes a new set that only contains the elements that are in this set
     * and the given set (the 'equal' elements).
     * 
     * @param other
     * @return 
     */
    public abstract CountSet<ElementT> intersect(Set<? super ElementT> other);
    
    /**
     * Makes a new set that contains all elements from this multi set without 
     * multiplicity.
     * 
     * @return 
     */
    public abstract Set<ElementT> withoutDuplicates();
    
    /**
     * Makes a map of this multi set.
     * The elements are the keys and the multiplicity is the value.
     * 
     * @return 
     */
    public abstract Map<ElementT, Integer> map();
    
    /**
     * The amount of different/distinct elements in this count set. 
     * This method should look like {@link Set#size}.
     * 
     * @return 
     */
    @Override
    public abstract int size();
    
    /**
     * The sum of all multiplicities.
     * @return The count of all elements including duplicates.
     */
    public abstract int cardinality();
}
