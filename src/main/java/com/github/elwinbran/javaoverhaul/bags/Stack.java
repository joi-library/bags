/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import java.util.Iterator;

/**
 * A stack ADT only allows access from the 'top' of the stack; compare with a 
 * stack of things in real life which can topple by taking something else than
 * the top.
 * There is however an exception, the {@link #iterator iterator()} method will
 * iterate all elements of the stack, without breaking the stack.
 * 
 * @author Elwin Slokker
 * @param <ElementT> The type of the stored element(s).
 */
public interface Stack<ElementT> extends Bag<ElementT>
{
    /**
     * The same as {@link #push}.
     * 
     * @param element the to be added element.
     * @return {@code true} if the element was succesfully added and 
     * {@code false} otherwise.
     */
    @Override
    public abstract boolean add(ElementT element);
    
    /**
     * 'Peeks' at the top element but leaves it there.
     * 
     * @return the element that is currently on the top of the stack.
     */
    public abstract ElementT peek();
    
    /**
     * Take the element of the top of the stack.
     * 
     * @return the current top element.
     */
    public abstract ElementT pop();
    
    /**
     * Adds the given element to the stack.
     * This operation might fail if the implementation has a capacity
     * constraint.
     * 
     * @param element the element to be pushed/added to the stack.
     * @return {@code true} if the object could be added and {@code false} in 
     * all other cases.
     */
    public abstract boolean push(ElementT element);
    
    public abstract boolean push(Iterable<? extends ElementT> iterable);
    
    
    /**
     * 'Stacks' the iterable on this one; the first element of the iterable 
     * becomes the new top of the stack and last element comes atop the old top.
     * 
     * @param stackable
     * @return 
     */
    public abstract boolean stack(Iterable<? extends ElementT> stackable);
    
    /**
     * Distance from top..?
     * @param sought
     * @return 
     */
    public abstract int search(ElementT sought);
}
