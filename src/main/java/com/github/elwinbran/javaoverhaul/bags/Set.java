/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

/**
 * A set is an ADT that only stores unique elements.
 * 
 * @author Elwin Slokker
 */
public interface Set<E> extends Bag<E>
{
    /**
     * Adds the element to the set if it does not exist yet.
     * 
     * @param element
     * @return 
     */
    @Override
    public abstract boolean add(E element);
    
    /**
     * Adds all given elements to the set.
     * 
     * @param iterable
     * @return 
     */
    @Override
    public abstract boolean addAll(Iterable<? extends E> iterable);
    
    public abstract void remove(E element);
    
    /**
     * Removes all elements from the set that are also present in the given 
     * iterable.
     * 
     * @param iterable
     * @return 
     */
    public abstract void remove(Iterable<? extends E> iterable);
    
    public abstract boolean contains(E element);
    
    /**
     * Makes a new set that only contains all elements of this set and the given
     * set combined.
     * @param other
     * @return 
     */
    public abstract Set<E> union(Set<? super E> other);
    
    /**
     * Makes a new set that contains all elements from this set, that are not 
     * also in the given subtractor set.
     * 
     * @param subtractor
     * @return 
     */
    public abstract Set<E> subtractComplement(Set<? super E> subtractor);
    
    /**
     * Makes a new set that only contains elements that are not present in both 
     * this set and the given set (the 'distinct' elements).
     * 
     * @param other
     * @return 
     */
    public abstract Set<E> difference(Set<? super E> other);
    
    /**
     * Makes a new set that only contains the elements that are in this set
     * and the given set (the 'equal' elements).
     * 
     * @param other
     * @return 
     */
    public abstract Set<E> intersect(Set<? super E> other);
    
    /**
     * Makes a multiset ADT out of this set and another.
     * @param other
     * @return 
     */
    public abstract CountSet<E> duplicateUnion(Set<? super E> other);
    
    //equality method for set comparison.
}
