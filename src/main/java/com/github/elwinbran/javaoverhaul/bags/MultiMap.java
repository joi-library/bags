
package com.github.elwinbran.javaoverhaul.bags;

import com.github.elwinbran.javaoverhaul.base.Pair;
import com.github.elwinbran.javaoverhaul.containers.Container;

/**
 * A very specific ADT that acts like a map with multiple values assigned to 
 * each key.
 * 
 * @author Elwin Slokker
 * @param <KeyT>
 * @param <ValueT>
 */
public interface MultiMap<KeyT, ValueT> extends 
        Bag<Pair<? extends KeyT, Container<? extends ValueT>>>
{
    /**
     * Adds the pair given that the key does not exist in the map yet.
     * Same as {@link #put(com.github.elwinbran.javaoverhaul.base.Pair)}
     * @param pair 
     * @return 
     */
    @Override
    public abstract boolean add(Pair<? extends KeyT, 
                                Container<? extends ValueT>> pair);
    
    public abstract boolean containsKey(KeyT key);
    
    public abstract void put(KeyT key, ValueT value);
    
    public abstract void put(Pair<KeyT, Container<ValueT>> keyValues);
    
    public abstract void putAll(Pair<KeyT, ValueT> pair);
    
    public abstract void putAll(MultiMap<? extends KeyT, ? extends ValueT> map);
    
    public abstract void putAll(
            Iterable<Pair<KeyT, Container<ValueT>>> map);
    
    public abstract Container<ValueT> get(KeyT key);
    
    public abstract boolean replace(KeyT key, ValueT oldValue, ValueT newValue);
    
    /**
     * Removes a key-values pair from this map and returning all values of that
     * key.
     * 
     * @param key
     * @return 
     */
    public abstract Container<ValueT> removeKey(KeyT key);
    
    public abstract boolean removeValue(KeyT key, ValueT value);
    
    public abstract Iterable<ValueT> getValues();
    
    public abstract Iterable<KeyT> getKeys();
}
