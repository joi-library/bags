/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

import com.github.elwinbran.javaoverhaul.bags.List;
import com.github.elwinbran.javaoverhaul.bags.Bag;

/**
 * A list ADT that offers access strictly through positions of elements.
 * 
 * @author Elwin Slokker
 * @param <ElementT> The type of the stored element(s).
 */
public interface IndexedList<ElementT> extends Bag<ElementT>
{
    public abstract boolean insert(ElementT element, int position);
    
    public abstract boolean insert(Iterable<? extends ElementT> iterable, 
                                   int position);
    /**
     * Replaces the value at the specified index by the specified value.
     * 
     * @param element
     * @param position
     * @return 
     */
    public abstract boolean set(ElementT element, int position);
    
    /**
     * Retrieves the element at the specified position in the list.
     * 
     * @param position
     * @return If the position exists in the list, the element that is placed on
     * that position, otherwise an exception will be thrown.
     */
    public abstract ElementT get(int position);
    
    public List<ElementT> subList(int fromIndex, int toIndex);
}
