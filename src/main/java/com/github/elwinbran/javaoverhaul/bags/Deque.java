/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import java.util.Iterator;

/**
 *
 * @author Elwin Slokker
 * @param <ElementT>
 */
public interface Deque<ElementT> extends Bag<ElementT>
{
    /**
     * {@#enqueueLast}
     * @param element
     * @return 
     */
    @Override
    public abstract boolean add(ElementT element);
    
    public abstract boolean enqueueFirst(ElementT element);
    
    public abstract boolean enqueueFirst(Iterable<? extends ElementT> iterable);
    
    public abstract NullableWrapper<ElementT> dequeueFirst();
    
    public abstract NullableWrapper<ElementT> peekFirst();
    
    public abstract boolean enqueueLast(ElementT element);
    
    public abstract boolean enqueueLast(Iterable<? extends ElementT> iterable);
    
    public abstract ElementT dequeueLast();
    
    public abstract ElementT peekLast();
}
