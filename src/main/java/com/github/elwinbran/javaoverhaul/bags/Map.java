/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.base.Pair;

/**
 * A map or 'mapping' is an ADT that stores key-value pairs.
 * Compare this with a table. You would like to know some value and look it
 * up via the column that is unique. That is a Map ADT.
 * 
 * @author Elwin Slokker
 * @param <KeyT> The type of the keys in this map.
 * @param <ValueT> The type of the values in this map.
 */
public interface Map<KeyT, ValueT> extends Bag<Pair<KeyT, ValueT>>
{
    /**
     * Will add the pair to the map, if the key is not a duplicate.
     * 
     * @param pair
     * @return 
     */
    @Override
    public abstract boolean add(Pair<KeyT, ValueT> pair);
    
    /**
     * A shortcut for iterating all pairs and looking up the sought key.
     * 
     * @param key The key that is checked.
     * @return {@code true} if the {@code key} has an entry in this map. 
     * {@code false} otherwise.
     */
    public abstract boolean containsKey(KeyT key);
    
    /**
     * Retrieves the value belonging to the sought {@code key} if it exists.
     * 
     * @param key the required value identifier.
     * @return the value belonging to the key pair, wrapped because the result 
     * is null of the key does not exist.
     */
    public abstract NullableWrapper<ValueT> get(KeyT key);
    
    /**
     * Add a pair to the map.
     * 
     * @param key the identifier of the pair.
     * @param value the value of the pair.
     * @return {@code true} if the pair was added, {@code false} otherwise.
     */
    public abstract boolean put(KeyT key, ValueT value);
    
    /**
     * Add a pair to the map.
     * 
     * @param pair an object containing full key-value pair.
     * @return {@code true} if the pair was added, {@code false} otherwise.
     */
    public abstract boolean put(Pair<KeyT, ValueT> pair);
    
    /**
     * Remove a pair based on the key.
     * 
     * @param key the pair identifier of the to be removed pair.
     * @return the value of the pair if the key could be found, {@code null} or
     * a runtime exception otherwise.
     */
    public abstract NullableWrapper<ValueT> remove(KeyT key);
    
    /**
     * Removes a pair from the map.
     * The key and value must match exactly.
     * 
     * @param pair The pair that needs to be removed.
     */
    public abstract void remove(Pair<KeyT, ValueT> pair);
    
    /**
     * Get all the keys of this map.
     * 
     * @return a sequence of keys, in no particular order.
     */
    public abstract Iterable<KeyT> getKeys();
    
    /**
     * 
     * Get all the values of this map.
     * 
     * @return a sequence of values, in no particular order.
     */
    public abstract Iterable<ValueT> getValues();
}
