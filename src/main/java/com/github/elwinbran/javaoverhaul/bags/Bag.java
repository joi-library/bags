/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

import com.github.elwinbran.javaoverhaul.containers.Container;

/**
 * The most generic mutable ADT.
 * It can remove all its elements (clear) and have elements added to it, in 
 * addition to the read.
 * 
 * @author Elwin Slokker
 * @param <ElementT> The type of the stored element(s).
 */
public interface Bag<ElementT>
{
    /**
     * Provides the current immutable state of this container.
     * 
     * @return An immutable sequence that includes all elements currently in 
     * the container.
     */
    public abstract Container<ElementT> currentElements();
    
    /**
     * @return The exact amount of elements stored in this container.
     */
    public abstract int size();
    
    /**
     * Checks if this container is empty.
     * May be overridden for optimization.
     * 
     * @return {@code true} if the container contains no elements, {@code false}
     * otherwise.
     */
    public default boolean isEmpty()
    {
        return size() == 0;
    }
    
    /**
     * Checks if this container contains the element.
     * Override for optimization.
     * 
     * @param element The element that is sought.
     * @return {@code true} if the container contains the sought element, 
     * {@code false} otherwise.
     */
    public default boolean contains(ElementT element)
    {
        for(ElementT containedElement : currentElements())
        {
            if(containedElement.equals(element))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Removes all elements and their references from this container.
     */
    public void clear();
    
    /**
     * Simply adds the given object to this container. 
     * How the object is inserted is up to the implementation.
     * Implementations may also decide to not add the object.
     * This can be decided for the sake of capacity or ADT constraints.
     * 
     * @param element The object that needs to be stored in this container.
     * @return {@code true} if the object could be added and {@code false} in 
     * all other cases.
     */
    public boolean add(ElementT element);
    
    /**
     * Very naive default add all method.
     * Override for optimization.
     * 
     * @param iterable
     * @return 
     */
    public default boolean addAll(Iterable<? extends ElementT> iterable)
    {
        boolean success = true;
        for(ElementT element : iterable)
        {
            if(!add(element))
            {
                success = false;
            }
        }
        return success;
    }
}
