/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.bags;

/**
 *
 * @author Elwin Slokker
 * @param <ElementT>
 * @param <PriorityT>
 */
public interface PriorityQueue<ElementT, PriorityT> extends Bag<ElementT>
{
    /**
     * 
     * @param element
     * @return 
     */
    @Override
    public boolean add(ElementT element);
    
    /**
     * 
     * @param iterable
     * @return 
     */
    @Override
    public boolean addAll(Iterable<? extends ElementT> iterable);
    
    public boolean enqueue(ElementT element, PriorityT priority);
    
    public boolean enqueue(Map<? extends ElementT, ? extends PriorityT> priorityTable);
    
    public ElementT dequeue();
    
    public ElementT peek();
}
